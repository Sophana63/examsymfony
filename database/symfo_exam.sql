
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `symfo_exam`
--


--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20220419070813', '2022-04-19 07:08:34', 299),
('DoctrineMigrations\\Version20220419072237', '2022-04-19 07:22:46', 207),
('DoctrineMigrations\\Version20220419075353', '2022-04-19 07:54:01', 151),
('DoctrineMigrations\\Version20220419085546', '2022-04-19 08:55:54', 219);



--
-- Déchargement des données de la table `forum`
--

INSERT INTO `forum` (`id`, `title`, `created_at`, `user_id`, `description`) VALUES
(1, 'Jeux', '2022-04-18 09:23:00', 1, 'Paragraph of text beneath the heading to explain the heading. We\'ll add onto it with another sentence and probably just keep going until we run out of words.'),
(2, 'Manga', '2022-03-02 09:23:00', 1, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.'),
(3, 'aaa', '2017-01-01 00:00:00', 1, 'aaaa'),
(4, 'hjjkhjkh', '2017-01-01 00:00:00', 1, 'hjkhjkhjkhjkhkj'),
(5, 'azeert', '2022-04-19 09:06:21', 1, 'zerzc zedzfzef');



INSERT INTO `message` (`id`, `user_id`, `content`, `created_at`, `forum_id`) VALUES
(1, 1, 'comment jeux 1', '2022-04-19 08:56:45', 1),
(2, 1, 'comment jeux 2', '2022-04-19 08:56:45', 1),
(3, 1, 'zaeazeazeazeaze daqazazea', '2022-04-19 09:49:00', 1),
(4, 1, 'zaeazeazeazeaze daqazazea', '2022-04-19 09:49:22', 1),
(5, 1, 'qqqqqqq', '2022-04-19 09:50:02', 1),
(6, 1, 'qqqqqqq', '2022-04-19 09:51:57', 1);



INSERT INTO `user` (`id`, `email`, `roles`, `password`) VALUES
(1, 'admin@admin.fr', '[\"ROLE_ADMIN\", \"ROLE_USER\"]', '$2y$13$/i4mJLwGA3KtFln4.uAKB.6kaGGTyEPk3Py8AmbkmSBlv53RAgFn2');


COMMIT;
