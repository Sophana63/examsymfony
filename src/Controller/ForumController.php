<?php

namespace App\Controller;

use DateTime;
use App\Entity\Message;
use App\Form\MessageType;
use App\Repository\ForumRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ForumController extends AbstractController
{
    #[Route('/forum/{id}', name: 'forum')]
    public function index($id, ForumRepository $forumRepo, Request $request, EntityManagerInterface $em): Response
    {

        $message = new Message();
        $forum = $forumRepo->findOneBy([
            'id' => $id,
        ]);
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);  
        if ($form->isSubmitted() && $form->isValid()) {
            $message = $form->getData();
            $message -> setUser($this->getUser());
            $date = new DateTime();
            $message -> setCreatedAt($date);            
            $message -> setForum($forum);

            $em->persist($message);
            $em->flush();

            $this->redirectToRoute("forum", [
                'id' => $id
            ]);
        }
        
        return $this->render('forum/index.html.twig', [
            'forum' => $forum,
            'form' => $form->createView(),
        ]);
    }
}
