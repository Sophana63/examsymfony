<?php

namespace App\Controller\Admin;

use App\Entity\Forum;
use App\Form\ForumType;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AdminController extends AbstractController
{
    #[Route('/', name: 'admin_admin')]
    public function index(Request $request, EntityManagerInterface $em): Response
    {
        $forum = new Forum();
        $form = $this->createForm(ForumType::class, $forum);
        $form->handleRequest($request);  
        if ($form->isSubmitted() && $form->isValid()) {
            $forum = $form->getData();
            $forum -> setUser($this->getUser());
            $date = new DateTime();
            $forum -> setCreatedAt($date);
            $em->persist($forum);
            $em->flush();

            $this->redirectToRoute("home");
        }
        return $this->render('admin/admin/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
