<?php

namespace App\Controller;

use App\Repository\ForumRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/home', name: 'home')]
    public function index(ForumRepository $forumRepo): Response
    {

        $forums = $forumRepo->findAll();
        return $this->render('home/index.html.twig', [
            'forums' => $forums,
        ]);
    }
    
}
