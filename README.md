Projet pour l'examen Symfony

## Création d'un forum
## + Utilsateur Admin, avec possibilité de créer un forum
## + Afficher les commentaires et ajout de commentaire

En local, créer le .env.local et mettre ces informations:

```
DATABASE_URL="mysql://root:@127.0.0.1:3306/symfo_exam?serverVersion=5.7&charset=utf8mb4"
```


Commande pour la création de la base de donnée:

```
symfony console d:d:c
```


Pour l'insertion de données dans la BDD, le fichier SQL (symfo_exam.sql) se trouve dans le dossier /database.
A exécuter sur PhpMyAdmin.


Lancer le serveur avec :
```
symfony serve
```


Voici les identifiants:
admin@admin.fr
admin
